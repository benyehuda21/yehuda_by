import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  // TODO: change this implementation from static to dynamic
  buildLocationRows(locations) {
    console.log(locations)
    return Object.keys(locations).map(function(key, i) {
      return (
        React.DOM.tr({key: i}, 
            React.DOM.td({key: 'name' + i}, locations[key].name),
            React.DOM.td({key: 'region' + i}, locations[key].region)
        )
      );
    })
  };


  // TODO: change the implementation of the add_location button to retrieve the name and region via form input elements
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>
        <form className="e-form">
          <div className="field">
            <input type="text" name="name" id="name" placeholder="Name"></input>
          </div>
          <div className="field">
            <input type="text" name="region" id="region" placeholder="Region"></input>
          </div>
          <button id="add_location" className="myButton"
            onClick={() => this.props.addLocation({ name: 'China', region: 'Asia' })}
            >
            Add Name and Region
          </button>
        </form>
        <table className="e-table">
          <thead>
            <tr><th>Name</th><th>Region</th></tr>
          </thead>
          <tbody>
            { this.buildLocationRows(this.props.locations) }
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
